package be.kdg.pro3.bootstrapgrid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootstrapGridApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootstrapGridApplication.class, args);
	}

}
